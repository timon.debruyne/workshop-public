function initGlobeEcharts() {
  var chart6 = echarts.init(document.getElementById('main6'));

  chart6.setOption({
    globe: {
      baseTexture: 'http://127.0.0.1:8887/asset/globe.jpeg',


      shading: 'color',

      light: {
        ambient: {
          intensity: 0.1
        },
        main: {
          intensity: 2
        }
      },

      atmosphere: {
        show: true
      },

      viewControl: {
        autoRotate: false
      },

      layers: []
    },
    series: []
  });


  var config = {
    color: '#fff'
  };

  function update() {
    chart6.setOption({
      globe: {
        atmosphere: {
          color: config.color
        }
      }
    })
  }
  var gui = new dat.GUI();
  gui.addColor(config, 'color').onChange(update);

  window.addEventListener('resize', function () {
    chart6.resize();
  });
}